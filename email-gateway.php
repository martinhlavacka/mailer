<?php

    /*
		CREDIT
		---------------------------------------
		Author: 	Martin Hlavacka
		Contact: 	martinhlavacka@outlook.com 
		Date: 		06.04.2018
				
		LICENSE
		---------------------------------------
		Permission is hereby granted, free of charge, to any person obtaining a copy
		of this software and associated documentation files (the "Software"), to deal
		in the Software without restriction, including without limitation the rights
		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
		copies of the Software, and to permit persons to whom the Software is
		furnished to do so, subject to the following conditions:
		The above copyright notice and this permission notice shall be included in all
		copies or substantial portions of the Software.
		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
		SOFTWARE.
		USAGE
		---------------------------------------
		1. Register on Sendgrid
		2. Get API token
		3. Update prepared values
	*/

    // PREPARE VALUES
    $mailer_api_token       = "API-TOKEN";
    $mailer_sender_name     = "NAME-OF-SENDER";
    $mailer_sender_email    = "E-MAIL-OF-SENDER";
    $mailer_receiver_name   = "SHARED-NAME-OF-RECEPINETS";
    $mailer_receiver_email  = ["E-MAIL-ADDRESS", "E-MAIL-ADDRESS"]; 
    $mailer_subject         = "SUBJECT-OF-THE-EMAIL";
    $mailer_message         = "BODY-OF-THE-EMAIL";

    // CREATE NEW OBJECT
    $email = new Email($mailer_api_token, $mailer_sender_name, $mailer_sender_email, $mailer_receiver_name, $mailer_receiver_email, $mailer_subject, $mailer_message);

    // DISPLAY RESULT
    echo "<pre>";
        print_r($email);
    echo "</pre>";

    // CLASS DEFINITION
    class Email{

        // PROPERTIES
        private $sender_name;
        private $sender_email;
        private $receiver_name;
        private $receiver_email;
        private $message_subject;
        private $message_body;
        private $api_token;
        private $sending_results_list_success;
        private $sending_results_list_error;

        // CONSTRUCTOR
        public function __construct($mailer_api_token, $mailer_sender_name, $mailer_sender_email, $mailer_receiver_name, $mailer_receiver_email, $mailer_subject, $mailer_message){

            // PREPARE ARRAY FOR RESULTS
            $this->sending_results_list_success = array();
            $this->sending_results_list_error = array();
 
            // LOOP THROUGH ALL RECEIVERS
            foreach ($mailer_receiver_email as $email_address){

                // ASSIGN VALUES
                $this->api_token = $mailer_api_token;
                $this->sender_name = $mailer_sender_name;
                $this->sender_email = $mailer_sender_email;
                $this->receiver_name = $mailer_receiver_name;
                $this->receiver_email = $email_address;
                $this->message_subject = $mailer_subject;
                $this->message_body = $mailer_message;
                
                // AUTOLOAD
                require __DIR__ . '/vendor/autoload.php';

                // SEND E-MAIL
                $sending_result = $this->sendEmail();

                // IF MAIL WAS SENT
                if($sending_result == 202) {
                    array_push($this->sending_results_list_success, $this->receiver_email);
                } 

                // IF SENDING FAILED
                else{
                    // print_r($response->headers());
                    // echo $response->body();
                    array_push($this->sending_results_list_error, $this->receiver_email);
                }

            } 
 
        }

        // SENDING E-MAILS
        public function sendEmail(){

            // API KEY
            $apiKey = $this->api_token;
            
            // SENDER AND RECEIVER
            $from       = new SendGrid\Email($this->sender_name, $this->sender_email);
            $to         = new SendGrid\Email($this->receiver_name, $this->receiver_email);
            
            // SUBJECT
            $subject    = $this->message_subject;
        
            // CONTENT
            $content    = new SendGrid\Content("text/html", $this->message_body);
            
            // SEND E-MAIL
            $mail = new SendGrid\Mail($from, $subject, $to, $content);
            $sg = new \SendGrid($apiKey);
            $response = $sg->client->mail()->send()->post($mail);
            $result = $response->statusCode();

            // RETURN RESULT
            return $result;

        }

    }

?>
